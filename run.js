const newman = require('newman');
const api_key = 'PMAK-615dd7da7d7e17003ff9573b-4e31409450ddc78f0a41226eccd1942df8'

const dev_col = '17315540-44dffe28-76bb-4024-8207-b4e3eba61d6e'
const dev_env = '17315540-b765e314-cb59-499b-a081-725bbb74db8a'
const dev_upd_col = '17315540-c155534c-b1fc-4bdd-8734-56d315010428'

const uat_col = '17315540-44dffe28-76bb-4024-8207-b4e3eba61d6e'
const uat_env = '17315540-31e61db1-cb09-490f-ab49-b7c3878e0f5a'
const uat_upd_col = '17315540-e478e8e5-4a63-4ac5-b6f2-cd1e26f11d2d'

// DEV 
newman.run({
        collection: `https://api.getpostman.com/collections/${dev_col}?apikey=${api_key}`,
        environment: `https://api.getpostman.com/environments/${dev_env}?apikey=${api_key}`,
        reporters: ['cli', 'htmlextra'],
        //iterationCount: 10,
        exportEnvironment: 'environment_dev.json',
        reporter: {
            htmlextra: {
                timezone: "Europe/Moscow",
            }
        }
    },
    () => {
        const fs = require("fs")
        const json = require("./environment_dev.json")
        //console.log(json)
        const env = {
            environment: json
        }
        const output = JSON.stringify(env)
        fs.writeFileSync("environment_dev.json", output)
        newman.run({
            collection: `https://api.getpostman.com/collections/${dev_upd_col}?apikey=${api_key}`,
            reporters: 'cli'
        }, () => {

            // UAT 
            newman.run({
                collection: `https://api.getpostman.com/collections/${uat_col}?apikey=${api_key}`,
                environment: `https://api.getpostman.com/environments/${uat_env}?apikey=${api_key}`,
                reporters: ['cli', 'htmlextra'],
                //iterationCount: 10,
                exportEnvironment: 'environment_uat.json',
                reporter: {
                    htmlextra: {
                        timezone: "Europe/Moscow",
                    }
                }
            }, () => {
                const json = require("./environment_uat.json")
                //console.log(json)
                const env = {
                    environment: json
                }
                const output = JSON.stringify(env)
                fs.writeFileSync("environment_uat.json", output)
                newman.run({
                    collection: `https://api.getpostman.com/collections/${uat_upd_col}?apikey=${api_key}`,
                    reporters: 'cli'
                });
            });
        });
    });